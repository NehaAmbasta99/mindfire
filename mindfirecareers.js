$(document).ready(function(){ 
		var err_day,err_dobyr,err_email,err_file,err_gradclg,err_gradscore,err_gradstream,err_gradyr,err_lgradclg,err_lgradscore,err_gradstream,
err_gradyr,err_lgradclg,err_lgradscore,err_lgradstream,err_lgradyr,err_lpostgradclg,err_lpostgradscore,err_postgradstream,err_postgradyr,
			err_score10,err_score12,err_skill1,err_skill2,err_skill3,err_mskill1,err_mskill2,err_mskill3,err_name,err_phone,err_postgradclg,err_postgradscore;
      
     $("#fresher").click(function() {
									$(".fresher1").show();
									$(".lessthan1year1 ").hide();
									$(".morethan1year1").hide();
									});
									
									
									
	$("#lessthan1Year").click(function(){ 
									$(".fresher1").hide();
									$(".lessthan1year1").show();
									$(".morethan1year1").hide();
									});
									
									
	$("#morethan1Year").click(function(){ 
									$(".fresher1").hide();
									$(".lessthan1year1").hide();
									$(".morethan1year1").show();
									});
									
		//validation ofemail address		
    $("#email").focusout( function () {
											var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
											var email=$('#email').val();
											var n= expr.test(email);
											
        if (!n) {
	
            $("#valemail").css("color", "yellow").text("Invalid email address!");
				var err_email=true;
       }
	   });
   //validation of name
   $("#name").focusout( function () {
											//var expr =/^[a-zA-Z]/;
											var name=$('#name').val();
											//n=expr.test(name);
											
        if (name=="") {
	
            $("#valname").css("color", "yellow").text("Enter name!");	
					var err_name=true;
       }
       
    });
	//validation of phone number
	 $("#phone").focusout( function () {
											 var expr =  /^\d{10}$/;
										//	 var pattern = /^\d{10}$/;
											var phone=$('#phone').val();
											var n=expr.test(phone);
											
        if (!n ) {
	
            $("#valphone").css("color", "yellow").text("Enter Phone number!");	
					var err_phone=true;
       }
	 });
	   //dob validation
	   $("#day").focusout(function(){
			var day = $('#day').val();

			if(day == "Day" )
			{
				$("#valdob").css("color", "yellow").text("Please select a Date of Birth");
				var err_day=true;
			}
       
    });
	
	$("#month").focusout(function(){
			
			var month=$('#month').val();
			if( month=="Month")
			{
				$("#valdob").css("color", "yellow").text("Please select a Date of Birth");
				var err_month=true;
			}
       
    });
	$("#dobyr").focusout(function(){
			
			var dobyr=$('#dobyr').val();
			if( dobyr=="Year")
			{
				$("#valdob").css("color", "yellow").text("Please select a Date of Birth");
				var err_dobyr=true;
			}
       
    });
	
   //validation of 10thscore
   $("#score10").focusout( function () {
											
											var score10=$('#score10').val();
										
											
			if (score10==""||score10<30) {
	
            $("#val10score").css("color", "yellow").text("Enter 10th score!");	
					err_score10=true;
       }
       
    });
	//validation of 12thscore
	$("#score12").focusout( function () {
											
											var score12=$('#score12').val();
										
											
			if (score12==""||score12<30) {
	
            $("#val12score").css("color", "yellow").text("Enter 12th score!");		
					err_score12=true;
       }
       
    });
	//validation of grad stream
	$("#gradstream").focusout( function () {
											
											var n=$('#gradstream').val();
										
											
			if (n==0) {
	
            $("#valgradstream").css("color", "yellow").css("font-size","10px").text("Select a stream!");
					err_gradstream=true;
       }
       
    });
	//validation of gradscore
	$("#gradscore").focusout( function () {
											
											var n=$('#gradscore').val();
										
											
			if (n==""||n<30) {
	
            $("#valgradscore").css("color", "yellow").css("font-size","10px").text("Enter Graduation score!");			
							err_gradscore=true;
	   }
	   
       
    });
	//validation of grad clg
	$("#gradclg").focusout( function () {
											
											var n=$('#gradclg').val();
										
											
			if (n=="") {
	
            $("#valgradclg").css("color", "yellow").css("font-size","10px").text("Enter College!");	
						err_gradclg=true;
       }
       
    });
	//validation of gradyr
	$("#gradyr").focusout( function () {
											
											var n=$('#gradyr').val();
										
											
			if (n=="") {
	
            $("#valgradyr").css("color", "yellow").css("font-size","10px").text("Enter Graduation Year!");	
							err_gradyr=true;
       }
       
    });
	
	//validation of postgrad stream
	$("#postgradstream").focusout( function () {
											
											var n=$('#postgradstream').val();
										
											
			if (n==0) {
	
            $("#valpostgradstream").css("color", "yellow").css("font-size","10px").text("Select a stream!");
							err_postgradstream=true;
       }
       
    });
	//validation of postgradscore
	$("#postgradscore").focusout( function () {
											
											var n=$('#postgradscore').val();
										
											
			if (n==""||n<30) {
	
            $("#valpostgradscore").css("color", "yellow").css("font-size","10px").text("Enter PG score!");		
								err_postgradscore=true;
       }
       
    });
	//validation of postgrad clg
	$("#postgradclg").focusout( function () {
											
											var n=$('#postgradclg').val();
										
											
			if (n=="") {
	
            $("#valpostgradclg").css("color", "yellow").css("font-size","10px").text("Enter College!");	
							err_postgradclg=true;
       }
       
    });
	//validation of postgradyr
	$("#postgradyr").focusout( function () {
											
											var n=$('#postgradyr').val();
										
											
			if (n=="") {
	
            $("#valpostgradyr").css("color", "yellow").css("font-size","10px").text("Enter PG Year!");		
					err_postgradyr=true;
       }
       
    });
	
	//validation of skill1
	 $("#skill1").focusout(function(){
			var n = $('#skill1').val();

			if(n == 0 )
			{
				$("#valskill1").css("color", "yellow").text("Please select key skill");
							err_skill1=true;
			}
       
    });
	//validation of skill2
	 $("#skill2").focusout(function(){
			var n = $('#skill2').val();

			if(n == 0)
			{
				$("#valskill2").css("color", "yellow").text("Please select second skill");
							err_skill2=true;
			}
       
    });
	//validation of skill3
	 $("#skill3").focusout(function(){
			var n = $('#skill3').val();

			if(n == 0 )
			{
				$("#valskill3").css("color", "yellow").text("Please select third skill");
				err_skill3=true;
			}
       
    });
	
	
   //validation of l10thscore
   $("#lscore10").focusout( function () {
											
											var score10=$('#lscore10').val();
										
											
			if (score10==""||score10<30) {
	
            $("#lval10score").css("color", "yellow").text("Enter 10th score!");	
						err_lscore10=true;
       }
       
    });
	//validation of l12thscore
	$("#lscore12").focusout( function () {
											
											var score12=$('#lscore12').val();
										
											
			if (score12==""||score12<30) {
	
            $("#lval12score").css("color", "yellow").text("Enter 12th score!");		
							err_lscore12=true;
       }
       
    });
	//validation of lgrad stream
	$("#lgradstream").focusout( function () {
											
											var n=$('#lgradstream').val();
										
											
			if (n==0) {
	
            $("#lvalgradstream").css("color", "yellow").css("font-size","8px").text("Select a stream!");	
								err_lgradstream=true;
       }
       
    });
	//validation of lgradscore
	$("#lgradscore").focusout( function () {
											
											var n=$('#lgradscore').val();
										
											
			if (n==""||n<30) {
	
            $("#lvalgradscore").css("color", "yellow").css("font-size","8px").text("Enter Graduation score!");	
							err_lgradscore=true;
       }
       
    });
	//validation of lgrad clg
	$("#lgradclg").focusout( function () {
											
											var n=$('#lgradclg').val();
										
											
			if (n=="") {
	
            $("#lvalgradclg").css("color", "yellow").css("font-size","8px").text("Enter College!");	
								err_lgradclg=true;
       }
       
    });
	//validation of lgradyr
	$("#lgradyr").focusout( function () {
											
											var n=$('#lgradyr').val();
										
											
			if (n=="") {
	
            $("#lvalgradyr").css("color", "yellow").css("font-size","8px").text("Enter Graduation Year!");	
									err_lgradyr=true;
       }
       
    });
	
	//validation of lpostgrad stream
	$("#lpostgradstream").focusout( function () {
											
											var n=$('#lpostgradstream').val();
										
											
			if (n==0) {
	
            $("#lvalpostgradstream").css("color", "yellow").css("font-size","8px").text("Select a stream!");	
									err_lpostgradstream=true;
       }
       
    });
	//validation of lgradscore
	$("#lpostgradscore").focusout( function () {
											
											var n=$('#lpostgradscore').val();
										
											
			if (n==""||n<30) {
	
            $("#lvalpostgradscore").css("color", "yellow").css("font-size","8px").text("Enter PG score!");	
								err_lpostgradscore=true;
       }
       
    });
	//validation of lgrad clg
	$("#lpostgradclg").focusout( function () {
											
											var n=$('#lpostgradclg').val();
										
											
			if (n=="") {
	
            $("#lvalpostgradclg").css("color", "yellow").css("font-size","8px").text("Enter College!");	
								err_lpostgradclg=true;
       }
       
    });
	//validation of lgradyr
	$("#lpostgradyr").focusout( function () {
											
											var n=$('#lpostgradyr').val();
										
											
			if (n=="") {
	
            $("#lvalpostgradyr").css("color", "yellow").css("font-size","8px").text("Enter PG Year!");	
								err_lpostgradyr=true;
       }
       
    });
	//validation of mskill1
	 $("#mskill1").focusout(function(){
			var n = $('#mskill1').val();

			if(n == 0 )
			{
				$("#mvalskill1").css("color", "yellow").text("Please select key skill");
				err_mskill1=true;
			}
       
    });
	//validation of mskill2
	 $("#mskill2").focusout(function(){
			var n = $('#mskill2').val();

			if(n == 0)
			{
				$("#mvalskill2").css("color", "yellow").text("Please select second skill");
				err_mskill2=true;
			}
       
    });
	//validation of mskill3
	 $("#mskill3").focusout(function(){
			var n = $('#mskill3').val();

			if(n == 0 )
			{
				$("#mvalskill3").css("color", "yellow").text("Please select third skill");
				err_mskill3=true;
			}
       
    });
	// Validation for file
	$("#file").mouseout(function(){
			var file = $('#file').val();
			var expr=/^(?:[\w]\:|\\)(\\[a-z_\-\s0-9\.]+)+\.(txt|gif|pdf|doc|docx|xls|xlsx)$/;
						n=	expr.test(file);

			if(!n )
			{
				$("#valfile").css("color", "yellow").text("Please choose a file");
				err_file=true;
			}
       
    });
	//Validation for submit button
		$("#form").submit(function(){
		if(err_name==true||err_email==true||err_phone==true||err_day==true||err_dobyr==true||err_month==true||err_gradclg==true||err_gradscore==true||
				err_gradstream==true||err_gradyr==true||err_lgradclg==true||err_lgradscore==true||err_lgradstream==true||err_lgradyr==true||
			err_lpostgradclg==true||err_lpostgradscore==true||err_lpostgradstream==true||err_postgradyr==true||err_postgradclg==true||
err_postgradscore==true||err_postgradstream==true||err_postgradyr==true||err_score10==true||err_score12==true||err_mskill1==true||
				err_mskill2==true||err_mskill3==true||err_skill1==true||err_skill2==true||err_skill3==true||err_file==true||err_lscore10==true||err_lscore12==true)
					alert('Hi,Your form has not been submitted successfully');
					
		else{
					alert('Hi,Your form has not been submitted successfully');
					
			}	
		});
	
	
	

});

