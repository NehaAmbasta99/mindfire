var validator = $("#add-conference").validate({
   ignore: ".ignore",
   rules: {
       city: {
           required: function() {
               if ($("#city-name").val()) {
                   return false;
               } else {
                   return true;
               }
           }
       },
       country: {
           required: function() {
               if ($("#country-name").val()) {
                   return false;
               } else {
                   return true;
               }
           }
       },
       "hiddenRecaptcha": {
           required: function() {
               if(grecaptcha.getResponse() == '') {
                   return true;
               } else {
                   return false;
               }
           }
       }
   }
});
P