
using System;

	namespace Interface
	{
		interface IMessage
		{
			void Send(string b);
			string Receive();
		}
	
	}