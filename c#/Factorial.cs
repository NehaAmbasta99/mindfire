//- The Factorial of a positive integer, n, is defined as the product of the sequence n, n-1, n-2, ...1 and the factorial of zero, 0, is defined as being 1. 
//Solve this using both loops and recursion.

using System;
class Factorial
	{
	static void Main(string[] args)
		{
			Console.WriteLine("Enter the number");
			int num=Convert.ToInt32(Console.ReadLine());
			int fact=Factorial1(num);
			Console.WriteLine(fact);
		}
	static int Factorial1(int n)
		{
		int f=n;
		if(f==1)
			return 1;
		else
				
			return n*Factorial1(n-1);
		}
	}