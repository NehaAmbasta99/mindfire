//: Write C# code to display the output as shown below:
//Results: 

//x value                 y value        	expressions                results

//10                        5                  x+=y                          x=15

//10                        5                  x-=y-2                        x=7

//10                        5                  x*=y*5                       x=250        

//10                        5                  x/=x/y                         x=5

//10                        5                  x%=y                          x=0
using System;
class Table
		{
		static void Main(string[] args)
		{
			
			//Console.Write("Enter x value");
			//Console.Write("Enter y value");
			//int x=Convert.ToInt32(Console.ReadLine());
			//int y=Convert.ToInt32(Console.ReadLine());
			int x=10;
			int y=5;
			Console.WriteLine("{0,-8}\t{1,-8}\tx+=y \t x={2,-8}",x,y,x+y);
			Console.WriteLine("{0,-8}\t{1,-8}\tx-=y-2 \t x={2,-8}", x, y,x-y+2);
			Console.WriteLine("{0,-8}\t{1,-15}\tx*=y*5 \t x={2,-8}", x, y, x*y*5);
			Console.WriteLine("{0,-8}\t{1,-8}\tx=x/y \t x={2,-8}", x, y, (float)x/(x/y));
			Console.WriteLine("{0,-20}\t{1,-8}\tx%=y \t x={2,-8}", x, y, x%y);
			Console.WriteLine("[{0, -25}]", "Microsoft"); // Left aligned 
			Console.WriteLine("[{0,  25}]", "Microsoft"); // Right aligned
			Console.WriteLine("[{0,   5}]", "Microsoft"); // Ignored, Microsoft is longer than 5 chars
			Console.WriteLine("[{0,   0}]", "Microsoft"); // Ignored, Microsoft is longer than 5 chars

		}
		}
		