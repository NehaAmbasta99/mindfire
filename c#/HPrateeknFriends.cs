//Prateek wants to give a party to his N friends on his birthday, where each friend is numbered from 1 to N. His friends are asking for a gift to come to the party
//, instead of giving him one. The cost of the gifts are given in the array Value where ith friend asks for a gift which has a cost Costi.

//But, Prateek has only X amount of money to spend on gifts and he wants to invite his friends which are in continuous range such that sum of the
// cost of the gifts of those friends will be exactly equal to X.

//If he can invite his friends, who can satisfy the above condition then, print YES otherwise print NO.

//Input:
//The first line contains a single integer T, denoting the number of test cases. In each test case, the following input will be present: - The next line contains two space-separated integers N and X, where N represents the number of friends and X represents amount of money which Prateek can spend on gifts. 
//- Next N line contains N integers, where ith line contains ith integer, which represents the Costi .

//Ouput
//Output exactly T lines, each containing the answer to the corresponding test case .

//Constraints:
//1 <= T <= 10
//1 <= N , Costi <= 106
//1 <= X <= 1012
/* INPUT__
1
5 12
1
3
4
5
2 Output-Yes*/ 
using System; 

class PrateeknFriends {
    static void Main(string[] args) {

   int i=0;
   char sep=' ';
   
   
   string[] arr=new string[10];
   Console.WriteLine("Enter the Test cases");
   int t=Convert.ToInt32(Console.ReadLine());
   for(i=1;i<=t;i++)
   	{
		int c=0;
		int sum=0;
		if(i<=10){
   		 string str=Console.ReadLine();
		foreach(string elem in str.Split(sep))
		//string elem in girl.Split(sep)
				{
					
					arr[c++]=elem;
				
				}
				
		int elem1=Convert.ToInt32(arr[0]);
		
			for(int j=0;j<elem1;j++)
					 {
						int num=Convert.ToInt32(Console.ReadLine());
						 sum=sum+num;
					 }
			  
			  
				int totcost=Convert.ToInt32(arr[1]);
				if(sum==totcost)
					 {
						 Console.WriteLine("Yes");
					 }
				 else
						 {
							 Console.WriteLine("No");
						 }
				}
				
			
   


		 
   	}
	
	
	       
    }
}
