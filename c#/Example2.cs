//Write C# program to print the table of characters that are equivalent to the Ascii codes from 1 to 122.
//The program will print the 10 characters per line.
using System;
class Example2
	{
	static void Main(string[] args)
		{
		int i=1;
		while(i<122)
		{
		Console.Write((char)i + "\t");
		if(i%10==0)
			{
			Console.WriteLine("\n");
			}
			i++;
		}
		}
	}