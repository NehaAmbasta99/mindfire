//Rotate the array by a pivot(here pivot is position)
using System;
class Rotatearray
{
	static void Main(string[] args)
		{
		int i , j,temp;
		Console.Write("Enter the size of the array");
		int size=Convert.ToInt32(Console.ReadLine());
		Console.Write("Enter the pivot around which array need to be rotated:");
		int pivot=Convert.ToInt32(Console.ReadLine());
		int[] a=new int[size];
		for(i=0;i<size;i++)
			{
			Console.WriteLine("Enter the elements");
			a[i]=Convert.ToInt32(Console.ReadLine());
			}
			//Rotating
		for(i=1;i<=pivot;i++)
			{
				temp=a[0];
				for(j=1; j<size; j++)
						a[j-1] = a[j];
				a[size-1] = temp;
				
			}
	
			//Printing
			Console.WriteLine("The elements after rotation are:" );
		for(i=0;i<size;i++)
			{
			Console.WriteLine(a[i]);
			
			}
				
		}
		
}