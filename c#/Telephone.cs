/*Create a base class, Telephone, and derive a class ElectronicPhone from
it. In Telephone, create a protected string member phonetype and a public method
Ring() which outputs a text message such as this: “Ringing the <phonetype>.” In
ElectronicPhone, the constructor should set the phonetype to “Digital.” In the Run()
method, call Ring() on the ElectronicPhone to test the inheritance.*/
using System;
public abstract class Telephone
	{
		protected string phonetype;
		public abstract void Ring();
			
		static void Main(string[] args)
			{
			ElectronicPhone ob=new ElectronicPhone();
			DigitalPhone dp=new DigitalPhone();
			TalkingPhone tp=new TalkingPhone();
			DigitalCellPhone dcp=new DigitalCellPhone();
			ob.Ring();
			dp.Ring();
			tp.Ring();
			dp.VoiceMail();
			dcp.VoiceMail();
			}
	}
	/*Phones these days do a lot more than ring, as you know. Add a method
to DigitalPhone called VoiceMail() that outputs the message “You have a message.
Press Play to retrieve.” Now add a new class, DigitalCellPhone, that derives from
DigitalPhone and implements a version of VoiceMail() that outputs the message
“You have a message. Call to retrieve.”*/
public class ElectronicPhone : Telephone
	{
	public ElectronicPhone()
		{
		phonetype="Digital";
		}
		public override void Ring()
		{
			Console.WriteLine("Hello");
		}
	}
public class  DigitalPhone: Telephone
	{
	public DigitalPhone()
		{
		phonetype="DigitalPhone";
		}
		public virtual void VoiceMail()
		{
			Console.WriteLine("You have a Message.Press Play to recieve");
		}
		public override void Ring()
		{
			Console.WriteLine("Hellothis is DigitalPhone"+phonetype);
		}
	}
	public class  DigitalCellPhone: DigitalPhone
	{
	
		public override void VoiceMail()
		{
			Console.WriteLine("You have a Message.Press call to retrieve");
		}
		
	}
public class TalkingPhone : Telephone
	{
	public TalkingPhone()
		{
		phonetype="TalkingPhone";
		}
		public override void Ring()
		{
			Console.WriteLine("Hello this is talking phone"+phonetype);
		}
	}
	
	
	