//Write a program that prints the numbers in the given range. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”. Print a new line after each string or number.

//Input Format First line will be the number of testcases, T. Next line will have T integers, denoted by N.

//Output Format For each testcase, print the number from 1 to N. But follow the rules given in the problem statement.

//Constraints

//1 <= T <= 10

//N is an integer.
//Input
//---------
//2
//3 15

//output
//-------------
//1
//2
//Fizz
//1
//2
//Fizz
//4
//Buzz
//Fizz
//7
//8
//Fizz
//Buzz
//11
//Fizz
//13
//14
//FizzBuzz
using System; 

class FizzBuzz {
    static void Main(string[] args) {

   int i,a,b;
   char sep=' ';
   Console.WriteLine("Enter the Test cases");
   int t=Convert.ToInt32(Console.ReadLine());
   for(i=1;i<=t;i++)
   	{
		int sum=0;
		if(i<=10){
   		 string str=Console.ReadLine();
		foreach(string elem in str.Split(sep))
		
		{
			
			string str1=elem;
			int elem1=Convert.ToInt32(str1);
				for(int j=1;j<=elem1;j++)
					{
						if((j%5==0)&&(j%3==0))
							Console.WriteLine("FizzBuzz");
						
						else
						if(j%5==0)
							{
							Console.WriteLine("Buzz");
							}
						else 
							if(j%3==0)
							{
							Console.WriteLine("Fizz");
							}
							
						else
						Console.WriteLine(j);
					}
		}
		Console.Readkey(); 
   	}
   
   
       
    }
}
}
