
//In this C# exercise, you are about to write C# program to display a matrix as shown below. The diagonal of the matrix fills with 0s. The lower side fills will -1s and the upper side fills with 1s.


// 0	1	1	1	1	 
//-1	0	1	1	1
//-1	-1	0	1	1
//-1	-1	-1	0	1
//-1	-1	-1	-1	0
using System;
class MatrixExample
	{
	static void Main(string[] args)
		{
		int i , j;
		for(i=0;i<4;i++)
		{
			for(j=0;j<4;j++)
					{
						if(i==j)
							Console.Write(0 + "\t");
						
						 
								else if(i>j)
							Console.Write(-1+ "\t");
						else	
							Console.Write(1+ "\t");
					}
		Console.WriteLine("\n");
		}
		}
	}
