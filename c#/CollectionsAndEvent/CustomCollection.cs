/*Task details:
-----------------------------------------------------------------

Problem #1
----------------------------------------------------------------------------------------

Create a Customer class having following properties.
1.CustomerId
2.FirstName
3.LastName
4.Age
5.Address

Create a custom generic collection class , which will be used for below operations.

1.Adding a customer to the custom generic collection.
2.Removing a customer from the custom generic collection.
3.Searching a customer in the custom generic collection.
4.Listing all the customers in the custom generic collection.
5.Sorting the customer collection based on custom order.


Create a console application that will support these above operations.
Users can input various details of a customer and can use the add,remove,search,sort and list
functionalities.



Problem #2
-----------------------------------------------------------------------------------------

Replace the custom generic collection class with List<T> and do the same operations.
*/
using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace ConsoleApplicationgen
{
    
    public abstract class CustomerObjectBase
    {
        protected Guid? _UniqueId;
		public static int a;
       
        public CustomerObjectBase()
        {
            
            _UniqueId = Guid.NewGuid();
        }

        
        public Guid? UniqueId
        {
            get
            {
                return _UniqueId;
            }
            set
            {
                _UniqueId = value;
            }
        }
		public string Field{ get; set; }
    }
public class Customer:CustomerObjectBase,IComparable
			{
				
				private string _FirstName = "";
				private string _LastName = "";
				private int _CustomerId ;
				private int _Age ;
				private string _Address = "";

				
				public Customer(int id,string first, string last,int age,string address)
				{
					_CustomerId=id;
					_FirstName = first;
					_LastName = last;
					_Age=age;
					_Address=address;
					
				}
			public int CustomerId
					{
						get
						{
							return _CustomerId;
						}
						set
						{
							_CustomerId = value;
						}
					}
			public string FirstName
					{
						get
						{
							return _FirstName;
						}
						set
						{
							_FirstName = value;
						}
					}
			public string LastName
					{
						get
						{
							return _LastName;
						}
						set
						{
							_LastName = value;
						}
					}
			public int Age
					{
						get
						{
							return _Age;
						}
						set
						{
							_Age = value;
						}
					}
			public string Address
					{
						get
						{
							return _Address;
						}
						set
						{
							_Address = value;
						}
					}
					
			
			
					public int CompareTo(object obj)
					{
						
						
						switch(CustomerObjectBase.a)
						{
							case 1:
										{
											Customer cust = obj as Customer;
											int c=string.Compare(FirstName,cust.FirstName);
											return c;
											//break;
										}
							case 2:
										{
											Customer cust = obj as Customer;
											if (cust.CustomerId < CustomerId)
											{
												return 1;
												
											}
											else if (cust.CustomerId > CustomerId)
											{
												return -1;
												
											}
										else 
											return 0;
										
										}
							case 3:
										{
											Customer cust = obj as Customer;
											if (cust.Age < Age)
											{
												return 1;
												
											}
											else if (cust.Age > Age)
											{
												return -1;
												
											}
										else 
											return 0;
										
										}
							case 4:
										{
											Customer cust = obj as Customer;
											int c=string.Compare(LastName,cust.LastName);
											return c;
											//break;
										}
							default:
									
										{
											Customer cust = obj as Customer;
											int c=string.Compare(FirstName,cust.FirstName);
											return c;
											//break;
										}
								
								
						}
						
					}
			}
			
    public class CustomerObjectCollection<T> : ICollection<T> where T : CustomerObjectBase
    {
        
        protected ArrayList _innerArray;
        
        protected bool _IsReadOnly;

       
        public CustomerObjectCollection()
        {
            _innerArray = new ArrayList();
        }

      
        public T this[int index]
        {
            get
            {
                return (T)_innerArray[index];
            }
            set
            {
                _innerArray[index] = value;
            }
        }

        
        public virtual int Count
        {
            get
            {
                return _innerArray.Count;
            }
        }
		public void Sort(int b)
		{
		CustomerObjectBase.a=b;	
		_innerArray.Sort();	
		}

       
        public virtual bool IsReadOnly
        {
            get
            {
                return _IsReadOnly;
            }
        }

       
        public virtual void Add(T CustomerObject)
        {
            _innerArray.Add(CustomerObject);
        }
		
        public virtual bool Remove(T CustomerObject)
        {
            bool result = false;
           
            for (int i = 0; i < _innerArray.Count; i++)
            {
             
                T obj = (T)_innerArray[i];
                if (obj.UniqueId == CustomerObject.UniqueId)
                {
                   _innerArray.RemoveAt(i);
                    result = true;
                    break;
                }
            }

            return result;
        }

       public bool Contains(T CustomerObject)
        {
            foreach (T obj in _innerArray)
            {
                
                if (obj.UniqueId == CustomerObject.UniqueId)
                {
                    return true;
                }
            }
            return false;
        }

      public virtual void CopyTo(T[] CustomerObjectArray, int index)
        {
            throw new Exception(
              "This Method is not valid for this implementation.");
        }

      
        public virtual void Clear()
        {
            _innerArray.Clear();
        }
        public virtual IEnumerator<T> GetEnumerator()
        {
            return new CustomerObjectEnumerator<T>(this);
        }
		 IEnumerator IEnumerable.GetEnumerator()
        {
            return new CustomerObjectEnumerator<T>(this);
        }
      
		}
	    public class CustomerObjectEnumerator<T> : IEnumerator<T> where T : CustomerObjectBase 
    {
        protected CustomerObjectCollection<T> _collection; 
        protected int index; 
        protected T _current; 

        public CustomerObjectEnumerator()
        {
            
        }
        public CustomerObjectEnumerator(CustomerObjectCollection<T> collection)
        {
            _collection = collection;
            index = -1;
            _current = default(T);
        }

       
        public virtual T Current
        {
            get
            {
                return _current;
            }
        }
        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        // Dispose method
        public virtual void Dispose()
        {
            _collection = null;
            _current = default(T);
            index = -1;
        }

        // Move to next element in the inner collection
        public virtual bool MoveNext()
        {
            if (++index >= _collection.Count)
            {
               
                return false;
            }
            else
            {
                _current = _collection[index];
            }
            
            return true;
        }

        public virtual void Reset()
        {
            _current = default(T); 
            index = -1;
        }
    }
	 public class CustomerComparer : IComparer<Customer> 
    {
        public int Compare(Customer x,Customer y)
        {
            return x.CompareTo(y);
        }
    }
	public class Publisher
	{
		public delegate void CollectionHandler();
			public event CollectionHandler Change;
			public  void OnNumChanged()
			  {
				 if (Change != null)
				 {
					Change();
				 }
				 
			  }
	}
	public  class Program
    {
		/*public delegate void CollectionHandler();
			public static event CollectionHandler Change;
			public static void OnNumChanged()
			  {
				 if (Change != null)
				 {
					Change();
				 }
				 else
				 {
					Console.WriteLine("Event fired!");
				 }
			  }
		/*public static void Main(string[] args)
        {
           
            CustomCollectionProg();

            Console.ReadKey();
            Console.Read();
        }*/
		public static void EventHandler()
		{
			Console.WriteLine("Event fired!");
		}
        public  void CustomCollectionProg()
        {
			
		
			
					var customerObjList = new CustomerObjectCollection<Customer>();
					Console.WriteLine("Enter the number of Customer");
					int n=Convert.ToInt32(Console.ReadLine());
					for(int i=0;i<n;i++)
							{
								Console.WriteLine("Enter the CustomerId");
								int id=Convert.ToInt32(Console.ReadLine());
								Console.WriteLine("Enter the FirstName");
								string first=Console.ReadLine();
								Console.WriteLine("Enter the LastName");
								string last=Console.ReadLine();
								Console.WriteLine("Enter the Age");
								int age=Convert.ToInt32(Console.ReadLine());
								Console.WriteLine("Enter the Address");
								string address=Console.ReadLine();
							 var Cus = new Customer(id,first,last,age,address);	
							   customerObjList.Add(Cus);
							    Publisher publish = new Publisher();
							   publish.Change+=EventHandler;
							   publish.OnNumChanged();
							   Console.WriteLine("======================================================");
							   Console.WriteLine("======================================================");
							}
							Label1:	Console.WriteLine("Enter \n 1 to delete \n 2 to sort \n 3 to search \n 4 to display \n 5 to Exit");
							int opt=Convert.ToInt32(Console.ReadLine());
			switch(opt)
			{
				
				case 1:	{
						Console.WriteLine("Enter the id of the person whose details is to be removed");
						int cusid=Convert.ToInt32(Console.ReadLine());
						foreach (var customer in customerObjList)
								{
									if(customer.CustomerId==cusid)
									{
										var elem=customer;
										customerObjList.Remove(elem);
										  Publisher publish = new Publisher();
							   publish.Change+=EventHandler;
							   publish.OnNumChanged();
									}
									
								}
							Console.WriteLine("Total count {0}", customerObjList.Count);
						Console.WriteLine("----------------");
						foreach (var customer in customerObjList)
						{
							Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
						}
						Console.WriteLine("======================================================");
						Console.WriteLine("======================================================");
								goto Label1;
						}
				case 2:
						{
						Console.WriteLine("Enter the field to be sorted: \n 1 by FirstName \n 2 by Id \n 3 by age \n 4 by lastname");
						int a=Convert.ToInt32(Console.ReadLine());
						customerObjList.Sort(a);
						 Publisher publish = new Publisher();
							   publish.Change+=EventHandler;
							   publish.OnNumChanged();
						Console.WriteLine("This is the sorted data");
						Console.WriteLine("Total count {0}", customerObjList.Count);
						Console.WriteLine("======================================================");
						foreach (var customer in customerObjList)
						{
							Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
						}
						Console.WriteLine("======================================================");
						Console.WriteLine("======================================================");
						goto Label1;
						}
				case 3:
						{
							Console.WriteLine("Enter the id of the person whose details is to be searched");
							int cusidsearch=Convert.ToInt32(Console.ReadLine());
							bool found=false;
							foreach (var customer in customerObjList)
									{
											var elem=customer;
											
										 if (customerObjList.Contains(elem))
												{
													if(elem.CustomerId==cusidsearch)
													{
														
														
																	{
																				Console.WriteLine("Search matched");
																				  Publisher publish = new Publisher();
																				   publish.Change+=EventHandler;
																				   publish.OnNumChanged();
																					Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
																					found=true;
																	}
																	break;
													}
												
												}
												
										
									}
									if(found==false)
												{
													Console.WriteLine("Customer doesn't exist ");
												}
									
												
									
							Console.WriteLine("======================================================");
							Console.WriteLine("======================================================");
						goto Label1;
						}
				
				case 4:
						{	
						Console.WriteLine("Total count {0}", customerObjList.Count);
						Console.WriteLine("----------------");
						foreach (var customer in customerObjList)
						{
							Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
						}
						  Publisher publish = new Publisher();
							   publish.Change+=EventHandler;
							   publish.OnNumChanged();
							Console.WriteLine("======================================================");
							Console.WriteLine("======================================================");
							goto Label1;
							
						}
				case 5:
						Environment.Exit(0);
						break;
				
				
			}
			
          
            //Console.WriteLine("after adding to list..");
          /* 
			
			 foreach (var customer in customerObjList)
            {
                Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
            }
			//Remove
            
            Console.WriteLine("after removing");
            
            Console.WriteLine("Total count {0}", customerObjList.Count);
            Console.WriteLine("----------------");

            foreach (var customer in customerObjList)
            {
                 Console.WriteLine(customer.CustomerId+ " " +customer.FirstName + " " + customer.LastName+ " " + customer.Age+ " " + customer.Address);
            }
			//Search
			
           */
        }
}}
	
	


		