//Write C# code to declare two integer variables, one float variable
//and one string variable and assign 10, 12.5, and "C# programming" to them respectively. Then display their values on the screen.
using System;
class Example1
			{
	static void Main(string[] args){
								int a=10;
								int b=5;
								float c=9.9f;
								string d="C# Programming";
								Console.WriteLine("a:"+a);
								Console.WriteLine("b:"+b);
								Console.WriteLine("c:"+c);
								Console.WriteLine("d:"+d);
									}
			}