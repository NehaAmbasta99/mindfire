//Program to implement abstractclass and interface concept in a employee class.
using System;
abstract class Nature
				{
					 public void Normal(){
													Console.WriteLine("Normal");
												}
					abstract public int Cool();
					abstract public int Arrogant();
					// public abstract string Name{get;set;}
				}
	public interface Profile
				{
				void Frontend();
				void Backend();
				void DBA();
				}
	class Employee1 : Nature , Profile
			{
			private string _name;
				public  string Name
				{
					get{
						return _name;
					}
					set
					{
						_name=value;
					}
				}
				
				
				
				
				public override int Cool(){
										return 10;
										}
				public override int Arrogant(){
										return 20;
										}
				public void Frontend(){
										Console.WriteLine("He is a perfect Frontend Programmer");
										}
				public void Backend(){
										Console.WriteLine("He is not a Backend Programmer");
										}
				public void DBA(){
										Console.WriteLine("He is not a DBA");
										}
			}
	class Employee2 : Nature , Profile
			{
						public  string Name{get;set;}
				public override int Cool(){
										return 20;
										}
				public override int Arrogant(){
										return 5;
										}
				public void Frontend(){
										Console.WriteLine("He is a not a Frontend Programmer");
										}
				public void Backend(){
										Console.WriteLine("He is  a Backend Programmer");
										}
				public void DBA(){
										Console.WriteLine("He is not a DBA");
										}
			}
	class Employee3 : Nature , Profile
			{
				
				private string _name;
				public  string Name
				{
					get{
						return _name;
					}
					set
					{
						_name=value;
					}
				}
				public override int Cool(){
										return 0;
										}
				public override int Arrogant(){
										return 10;
										}
				public void Frontend(){
										Console.WriteLine("He is not a  Frontend Programmer");
										}
				public void Backend(){
										Console.WriteLine("He is not a Backend Programmer");
										}
				public void DBA(){
										Console.WriteLine("He is a DBA");
										}
			}
class AbstractInterface
   {
      static void Main(string[] args)
      {
		  
		  //For Employee1
         Employee1 t1 = new Employee1();
		 t1.Normal();
        t1.Name="Harshit";
		Console.WriteLine(t1.Name);
        Console.WriteLine("He is this much % cool:"+t1.Cool());		
		Console.WriteLine("He is this much % arrogant:"+t1.Arrogant());
		t1.Frontend();
		t1.Backend();
		t1.DBA();
		//For employee2
		 Employee2 t2 = new Employee2();
		  t2.Normal();
        t2.Name="Neha";
		Console.WriteLine(t2.Name);
        Console.WriteLine("She is this much % cool:"+t2.Cool());		
		Console.WriteLine("She is this much % arrogant:"+t2.Arrogant());
		t2.Frontend();
		t2.Backend();
		t2.DBA();
		 //For employee3
		 Employee3 t3 = new Employee3();
		  t3.Normal();
        t3.Name="Shivani";
		Console.WriteLine(t1.Name);
        Console.WriteLine("She is this much % cool:"+t3.Cool());		
		Console.WriteLine("She is this much % arrogant:"+t3.Arrogant());
		t3.Frontend();
		t3.Backend();
		t3.DBA();
         Console.ReadKey();
      }
   }